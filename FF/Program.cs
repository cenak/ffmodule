﻿#define TESTBUILD

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.IO;
using System.Drawing;

using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Cvb;
using Emgu.Util;
using Emgu.CV.UI;

using Newtonsoft.Json;

namespace FF
{
    //[Serializable]
    class Program
    {
        

        static void Main(string[] args)
        {
            DetectionFace.DetectFace(args[0]);
        }

        
    }

    class DetectionFace
    {
        static Image<Bgr, byte> imgInput;
        static List<Point> positionsOfFaces = new List<Point>();// = new Point[1000];

        static public void DetectFace(string path)
        {

            imgInput = new Image<Bgr, byte>(path);

            string facePath = Path.GetFullPath(@"../../data2/haarcascade_frontalface_default.xml");
            CascadeClassifier classifierFace = new CascadeClassifier(facePath);

            var imgGray = imgInput.Convert<Gray, byte>().Clone();
            Rectangle[] faces = classifierFace.DetectMultiScale(imgGray, 1.1, 4);
            
            int i = 0;

            foreach (var face in faces)
            {
                //positionsOfFaces[i++] = face.Location;
                positionsOfFaces.Add(face.Location);

#if TESTBUILD
                imgInput.Draw(face, new Bgr(0, 0, 255), 2);
#endif
            }

#if TESTBUILD
            imgInput.Save("Founded_" + path);
#endif

            //if (!MakeJSONFile())
            //    File.WriteAllText("log.txt", "Error: can't make or write in json file");

            MakeJSONFile();
        }

        static bool MakeJSONFile()
        {
            string jsonData = JsonConvert.SerializeObject(positionsOfFaces);

            File.WriteAllText("PositionOfDetectedFaces.json", jsonData);

            return true;
        }
    }
}
